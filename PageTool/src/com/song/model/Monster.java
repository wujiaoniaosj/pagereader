package com.song.model;

import com.alibaba.fastjson.JSONObject;

public class Monster {
    private int id;
    private int def;
    private int icon;
    private float hp;
    private int atkPer;

    @Override
    public String toString() {
        return "Monster{" +
                "id=" + id +
                ", def=" + def +
                ", icon=" + icon +
                ", hp=" + hp +
                ", atkPer=" + atkPer +
                '}';
    }

    public Monster(JSONObject monster) {
        this.id = monster.getInteger("ID");
        this.def = monster.getInteger("def");
        this.icon = monster.getInteger("icon");
        this.hp = monster.getFloat("hp");
        this.atkPer = monster.getInteger("atk_per");
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDef(int def) {
        this.def = def;
    }

    public void setIcon(int icon) {
        this.icon = icon;
    }

    public void setHp(float hp) {
        this.hp = hp;
    }

    public void setAtkPer(int atkPer) {
        this.atkPer = atkPer;
    }

    public int getId() {
        return id;
    }

    public int getDef() {
        return def;
    }

    public int getIcon() {
        return icon;
    }

    public float getHp() {
        return hp;
    }

    public int getAtkPer() {
        return atkPer;
    }
}
