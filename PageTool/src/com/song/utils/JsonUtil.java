package com.song.utils;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.song.model.Monster;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JsonUtil {
    public static List<Monster> jsonParser(String jsonContent){
        List<Monster> monsterList = new ArrayList<Monster>();
        //"1203":{"ID":1203,"type":1,"model":3005,"prefab":3005,"icon":3005,"reward":"1|0|442;3|0|3","atk":755.48,"hp":392.83,"def":0,"melee_def":0,"remote_def":0,"atk_per":0,"hp_per":0,"def_per":0,"atk_speed":1.02,"move_speed":100,"crit":0,"crit_def":0,"hit":0,"dodge":0,"critratio":0,"atk_range":38.25,"animation":61,"skill":0,"model_zoom":0.765,"name":108013005}
        JSONObject jo = JSONObject.parseObject(jsonContent);
        for(String key : jo.keySet()){
            JSONObject jo1 = jo.getJSONObject(key);
            Monster monster = new Monster(jo1);
            monsterList.add(monster);
        }
        return monsterList;
    }

    public static List<Monster> jsonParserArray(String jsonContent){
        List<Monster> monsterList = new ArrayList<Monster>();
        //"1203":{"ID":1203,"type":1,"model":3005,"prefab":3005,"icon":3005,"reward":"1|0|442;3|0|3","atk":755.48,"hp":392.83,"def":0,"melee_def":0,"remote_def":0,"atk_per":0,"hp_per":0,"def_per":0,"atk_speed":1.02,"move_speed":100,"crit":0,"crit_def":0,"hit":0,"dodge":0,"critratio":0,"atk_range":38.25,"animation":61,"skill":0,"model_zoom":0.765,"name":108013005}
        JSONArray jo = JSONArray.parseArray(jsonContent);
        Iterator it = jo.iterator();
        while(it.hasNext()){
            Monster monster = new Monster((JSONObject) it.next());
            monsterList.add(monster);
        }
//        for(String key : jo.iterator()){
//            JSONObject jo1 = jo.getJSONObject(key);
//            Monster monster = new Monster(Integer.parseInt(key),jo1);
//            monsterList.add(monster);
//        }
        return monsterList;
    }
}
