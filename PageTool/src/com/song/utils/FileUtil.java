package com.song.utils;
import java.io.*;
import java.util.Scanner;

public class FileUtil {
    public static String readFile(String path){
        String fileContent = "";
        try {
            File myObj = new File(path);
//            Scanner myReader = new Scanner(myObj);
            FileReader fr = new FileReader(myObj);
            BufferedReader br = new BufferedReader(fr);
            String line = null;
            while ((line=br.readLine())!=null) {
                fileContent += line;
            }
            br.close();
        } catch (Exception e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
        return fileContent;
    }
}
