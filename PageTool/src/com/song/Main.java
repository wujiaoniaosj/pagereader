package com.song;

import com.song.model.Monster;
import com.song.utils.FileUtil;
import com.song.utils.JsonUtil;

import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        String path = "resources/MonsterTable2.json";
        String fileContent = FileUtil.readFile(path);
//        System.out.println(fileContent);
        List<Monster> monsterList = JsonUtil.jsonParserArray(fileContent);
        Iterator<Monster> it = monsterList.listIterator();
        while(it.hasNext()){
            System.out.println(it.next());
        }
    }
}
